﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GeneticAlgorithm{

    class DNA {

        //in this class we have to values for fitness in order to check fitness much better and avoid possible 
        //errors in find a single fitness val
        public List<int> sumPile { get; }
        public List<int> multiplyPile { get; }
        public int sumFitnessVal { get; private set; }
        public int multiplyFitnessVal { get; private set; }
        public DNA(List<int> sumPile, List<int> multiplyPile) {
            this.multiplyPile = multiplyPile;
            this.sumPile = sumPile;
            this.multiplyFitnessVal = multiplyFitness();
            this.sumFitnessVal = sumFiteness();
        }

        private int sumFiteness(){
            //max value for fitness of sumPile is 40
            //best value is 36
            int sum = 0;
            foreach (var e in sumPile)
                sum += e;
            return sum;
        }

        private int multiplyFitness(){
            //max value for fitness of multiplyPile is 30240
            //best value is 360
            int product = 1;
            foreach (var e in multiplyPile)
                product *= e;
            return product;
        }

    }

    class GeneticSolver{
        private Random rnd;
        public GeneticSolver(){
            rnd = new Random();
        }

        private List<DNA> makeInitialPopulation(){
            List<DNA> initialPopulation = new List<DNA>();
            for (int i = 1; i <= 10; i++){
                List<int> sumPile = new List<int>();
                List<int> multiplyPile = new List<int>();
                while(sumPile.Count<5){
                    int x = rnd.Next(1,11);         //generates random num from 1 to 10
                    if (!sumPile.Contains(x) &&!multiplyPile.Contains(x))
                        sumPile.Add(x);
                }
                while(multiplyPile.Count < 5){
                    int x = rnd.Next(1, 11);         //generates random num from 1 to 10
                    if (!multiplyPile.Contains(x) && !sumPile.Contains(x))
                        multiplyPile.Add(x);
                }
                DNA dna = new DNA(sumPile,multiplyPile);
                initialPopulation.Add(dna);
            }
            return initialPopulation;
        }

        private DNA crossOver(DNA p1,DNA p2){
            //we get random genes from parents' dna to make new children
            List<int> sumPile = new List<int>();
            List<int> multiplyPile = new List<int>();
            List<int> newSumpile=new List<int>();
            List<int> newMultiplyPile= new List<int>();

            //copy sumplie genes of both parents to a list
            foreach (var d in p1.sumPile)
                sumPile.Add(d);
            foreach (var d in p2.sumPile)
                sumPile.Add(d);
            //copy multiplyPile genes of both parents to a list
            foreach (var d in p1.multiplyPile)
                multiplyPile.Add(d);
            foreach (var d in p2.multiplyPile)
                multiplyPile.Add(d);
            //at first we make child's sumPile
            while (newSumpile.Count < 5){
                int r = rnd.Next(sumPile.Count);//randon num from zero to sumPile.Count 
                if (!newSumpile.Contains(sumPile[r])){
                    newSumpile.Add(sumPile[r]);
                    sumPile.Remove(sumPile[r]);
                }
            }
            //then in order to stop getting stuck in infinite loop we add the remaining elements of
            //sumPile to multiplyPile
            foreach (var v in sumPile)
                multiplyPile.Add(v);
            while (newMultiplyPile.Count < 5) {
                int r = rnd.Next(multiplyPile.Count);
                if (!newMultiplyPile.Contains(multiplyPile[r]) && !newSumpile.Contains(multiplyPile[r])) {
                    newMultiplyPile.Add(multiplyPile[r]);
                    multiplyPile.Remove(multiplyPile[r]);
                }
            }
            DNA dna = new DNA(newSumpile, newMultiplyPile);
            return mutate(dna);
        }

        private DNA mutate(DNA dna){
            //as we know that there are no two genes with the same number in 
            //our lists we just swap one number of one of the lists with the other one
            int r = rnd.Next(101);
            //mutate DNA with a chance less than 40 percent
            if (r <= 40){
                List<int> sumPile = dna.sumPile;
                List<int> multiplyPile = dna.multiplyPile;
                int r1 = rnd.Next(5);
                int temp= sumPile[r1];
                sumPile[r1]=multiplyPile[r1];
                multiplyPile[r1] = temp;
                return new DNA(sumPile,multiplyPile);
            }
            else return dna;
        }

        public void solve(){
            List<DNA> population = makeInitialPopulation();
            DNA dna=null;
            bool foundAnswer = false;
            int generation = 1;

            while (!foundAnswer){
                List<DNA> newPopulation = new List<DNA>();
                Console.WriteLine("generation ::"+generation.ToString());
                showPopulation(population);
                //check if we have the best DNA
                dna = isAnswer(population);
                if (dna != null){
                    foundAnswer = true;
                    Console.WriteLine();
                    Console.WriteLine("generation ::" + generation.ToString());
                    Console.WriteLine("answer found");
                    showDNA(dna);
                    return;
                }

                //in each generation, only 70% of people can do a crossOver action
                int m = (population.Count * 70) / 100;

                for (int i = 0; i <m; i++){
                    //as we have set our population to be 15 at max 
                    int r1 = rnd.Next(population.Count);//random number from 0 to population size
                    int r2 = rnd.Next(population.Count);
                    newPopulation.Add(crossOver(population[r1],population[r2]));
                }
                
                //add new population to the main population
                foreach (var p in newPopulation)
                    population.Add(p);
                
                //we do a selection operation if there is any overpopulation 
                population= selection(population);
                generation++;

            }
        }
        private DNA isAnswer(List<DNA> population){
            //if we have any gene with zero fitness then we have found an answer
            foreach (var p in population)
                if (p.sumFitnessVal == 36 && p.multiplyFitnessVal==360)
                    return p;
            return null;
        }
        private List<DNA> selection(List<DNA> population){
            //if our population grows more than 17 people we do a selection
            //in the selection we chooes the people with sum fitnessVal more than 25
            //and people with multiply fitness val between 200 and 1000 
            if (population.Count > 17){
                List<DNA> selectedPopulation = new List<DNA>();
                foreach (var p in population){
                    if (p.sumFitnessVal>=25 && p.multiplyFitnessVal>=250 && p.multiplyFitnessVal<=1000)
                        selectedPopulation.Add(p);
                }
                return selectedPopulation;
            }
            else return population;
        }
        private void showDNA(DNA dna){
            Console.Write("Multiply Pile:: ");
            foreach (var d in dna.multiplyPile)
                Console.Write(d+" ");
            Console.WriteLine(" ::" + dna.multiplyFitnessVal.ToString());
            Console.Write("Sum Pile:: ");
            foreach (var d in dna.sumPile)
                Console.Write(d+" ");
            Console.WriteLine(" ::"+dna.sumFitnessVal.ToString());
            Console.WriteLine();
        }
        private void showPopulation(List<DNA> population){
            foreach (var p in population)
                showDNA(p);
        }

    }
}
