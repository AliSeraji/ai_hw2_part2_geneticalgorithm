﻿using System;

namespace GeneticAlgorithm{
    class Program{
        
        static void Main(string[] args){
            GeneticSolver geneticSolver = new GeneticSolver();
            geneticSolver.solve();
            Console.ReadLine();
            
        }
    }
}
